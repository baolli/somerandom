import sys
import json
import re


def main():

    tweet_file = open(sys.argv[1])

    tophash = {}
    count = 0
    for line in tweet_file:
        data = json.loads(line)

        default = ''

        if ('entities' in data) and (data.get('lang', default) == 'en'):
            count = count + 1
            text_list = data.get('entities', default)
            text_list = text_list.get('hashtags', default)
            for json_dict in text_list:
                hash_tag = json_dict.get('text').encode('utf-8')
                if hash_tag in tophash:
                    tophash[hash_tag] = tophash[hash_tag] + 1
                else:
                    tophash[hash_tag] = 1

    for i in range(10):
        key,value = max(tophash.iteritems(), key=lambda x:x[1])
        tophash[key] = 0
        print key, value
    print count

if __name__ == '__main__':
    main()
