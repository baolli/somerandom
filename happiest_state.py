import sys
import json
import re


def convert_to_words(param):
    "Convert string into list of words"
    list_of_words = re.sub("[^\w]", " ", param).split()
    return list_of_words


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])

    scores = {}
    st = {}
    states = {}
    tweet_amount ={}

    for line in sent_file:
        term, score = line.split("\t")
        scores[term] = int(score)

    for line in tweet_file:

        data = json.loads(line)

        default = ''
        place = data.get('place', {})
        if place:

            score = 0.0

            text_list = data.get('text', default).encode('utf-8')
            text_list = convert_to_words(text_list)

            for word in text_list:
                if word in scores:
                    score = score + scores.get(word)

            state_name = place.get('full_name', {}).encode('utf-8')
            state_name = convert_to_words(state_name)
            country_name = place.get('country', {})

            if state_name and country_name == 'United States':
                if state_name[-1] in states:
                    states[state_name[-1]] = states[state_name[-1]] + score
                    tweet_amount[state_name[-1]] = tweet_amount[state_name[-1]] + 1
                else: 
                    states[state_name[-1]] = score
                    tweet_amount[state_name[-1]] = 1.0
                    
    for rec in states:
        states[rec] = states[rec] / tweet_amount[rec]

    key, value = max(states.iteritems(), key=lambda x:x[1])
    print key

if __name__ == '__main__':
    main()
