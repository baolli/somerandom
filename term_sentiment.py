import json
import sys
import re
import pprint


def convert_to_words(param):
    "Convert string into list of words"
    list_of_words = re.sub("[^\w]", " ", param).split()
    return list_of_words


def main():
    sent_file = open(sys.argv[1])
    tweet_file = open(sys.argv[2])

    scores = {}
    new_scores = {}
    for line in sent_file:
        term, score = line.split("\t")
        scores[term] = int(score)

    for line in tweet_file:
        score = 0
        data = json.loads(line)

        default = ''
        text_list = data.get('text', default).encode('utf-8')
        text_list = convert_to_words(text_list)

        for word in text_list:
            if word in scores:
                score = score + scores.get(word)

        if score > 0:
            for word in text_list:
                if word not in scores:
                    new_scores[word] = 1.0
        elif score < 0:
            for word in text_list:
                if word not in scores:
                    new_scores[word] = -1.0
                    
    for key, value in new_scores.iteritems():
        print key, value


if __name__ == '__main__':
    main()
