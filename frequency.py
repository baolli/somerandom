import sys
import json
import re


def convert_to_words(param):
    "Convert string into list of words"
    list_of_words = re.sub("[^\w]", " ", param).split()
    return list_of_words


def main():

    tweet_file = open(sys.argv[1])
    total_amount = 0
    terms = {}

    for line in tweet_file:
        data = json.loads(line)

        default = ''
        text_list = data.get('text', default).encode('utf-8')
        text_list = convert_to_words(text_list)

        for word in text_list:
            total_amount += 1
            if word in terms:
                terms[word] = terms[word] + 1
            else:
                terms[word] = 1.

    for key, value in terms.iteritems():
        print key, value/total_amount


if __name__ == '__main__':
    main()
